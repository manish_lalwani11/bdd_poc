class CucumbersBasket:
    def __init__(self, initial_count=0):
        if initial_count < 0:
            raise ValueError("Initial count cannot be negative")

        self._count = initial_count

    @property
    def count(self):
        return self._count

    # @property
    # def isempty(self):
    #     return self._count == 0

    @count.setter
    def count(self,value):
        self._count = value

    def add(self, count):
        self._count += count

    def remove(self,count):
        self._count -= count



