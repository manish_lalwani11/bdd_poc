Feature: Cucumber Basket
  As a gardener,
  Want to carry cucumbers in a basket
  so that I don't drop them

#parameters dynamically 5,2,7 multiple usecases(different numbers)
  #multiple when and (actions)

   Scenario Outline: Add cucumbers to a basket
    Given The basket has <initial_cnt> cucumbers
    When <additional_cnt> cucumbers are added
    Then Total number of cucumbers in basket is <total_cnt>

     Examples: Values
     | initial_cnt | additional_cnt | total_cnt |
     | 2           | 5              | 7         |
     | 6           | 3              | 9         |
     | 25          | 5              | 30        |
     | 2           | 1              | 3         |


  Scenario: Remove cucumbers from basket
    Given The basket has 8 cucumbers
    When 2 cucumbers are removed
    Then Total number of cucumbers in basket is 6


    #multiple when then
  Scenario: Addition and Removal of cucumbers from basket
    Given The basket has 8 cucumbers
    When 2 cucumbers are removed
    When 5 cucumbers are added
    Then Total number of cucumbers in basket is 11