from pytest_bdd import scenario, given, when, then,parsers

from src.cucumbersbasket import CucumbersBasket


# @pytest.fixture
# def cucumber():
#     return CucumbersBasket(initial_count=0)

@scenario(features_base_dir="tests/features/", feature_name="cucumbers.feature",scenario_name="Add cucumbers to a basket")
def test_check_total():
    print("Ran")


@scenario(features_base_dir="tests/features/",feature_name="cucumbers.feature",scenario_name="Remove cucumbers from basket")
def test_check_remove_total():
    print("Ram remove")


@scenario(features_base_dir="tests/features/",feature_name="cucumbers.feature",scenario_name="Addition and Removal of cucumbers from basket")
def test_check_add_and_remove_total():
    print("Ran Addition and removal")

#@given(name="The basket has 2 cucumbers", target_fixture='basket')
#@given("The basket has <initial_cnt>",target_fixture='basket')
@given(parsers.cfparse("The basket has {initial_count:n} cucumbers",extra_types={'n': int}),target_fixture='basket')
def basket(initial_count):
    print("Initial_count is",initial_count)
    return CucumbersBasket(initial_count=initial_count)


@when(parsers.cfparse("{additional_count:n} cucumbers are added",{'n':int}))
def adding_cucumbers_to_basket(basket,additional_count):
    print("Additional_count ",additional_count)
    basket.add(additional_count)


@when(parsers.cfparse("{removal_count:n} cucumbers are removed",{'n':int}))
def removing_cucumbers_from_basket(basket,removal_count):
    basket.remove(removal_count)

@then(parsers.cfparse("Total number of cucumbers in basket is {total_count:n}",{'n':int}))
def total_number_of_cucumbers(basket,total_count):
    print("total_count is",total_count)
    assert basket.count == total_count
